package mulya.efendi.appx09

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context): SQLiteOpenHelper(context,DB_Name,null,DB_Ver) {
    companion object{
        val DB_Name = "media"
        val DB_Ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val tVid = "create table video(id_video text not null, id_cover text not null, video_title text not null)"
        val insVid = "insert into video values ('0x7f0c0003','0x7f06005f','Distorted Tea')," +
                "('0x7f0c0004','0x7f060060','OP Warnet')," +
                "('0x7f0c0005','0x7f060061','Ching Chong')"
        db?.execSQL(tVid)
        db?.execSQL(insVid)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }
}
