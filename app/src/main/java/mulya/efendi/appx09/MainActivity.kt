package mulya.efendi.appx09

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(){

    lateinit var mediaController: MediaController
    lateinit var db :SQLiteDatabase
    lateinit var lsAdapter: ListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        db = DBOpenHelper(this).writableDatabase

        mediaController = MediaController(this)
        mediaController.setAnchorView(videoView)
        videoView.setMediaController(mediaController)
        lv.setOnItemClickListener(itemClick)
    }

fun showDataVideo(){
    val cursor : Cursor = db.query("video", arrayOf("id_video as _id","id_cover","video_title"),
        null,null,null,null,"video_title asc")
    lsAdapter = SimpleCursorAdapter(this,R.layout.item_data_video,cursor,
        arrayOf("_id","id_cover","video_title"), intArrayOf(R.id.txIdVideo, R.id.txIdCover, R.id.txVideoTitle),
        CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
    lv.adapter = lsAdapter
}

    override fun onStart() {
        super.onStart()
        showDataVideo()
    }
    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        txJudulVideo.setText("Now Playing : "+c.getString(c.getColumnIndex("video_title")))
        imV.setImageResource(c.getInt(c.getColumnIndex("id_cover")))
        videoView.setVideoURI(Uri.parse("android.resource://"+packageName+"/"+c.getInt(c.getColumnIndex("_id"))))
    }

}
